$(function() {
    function showModal(params) {
        var modal         = $('#modal'),
            modal_dialog  = modal.find('.modal-dialog'),
            modal_content = modal.find('.modal-content'),
            modal_body, modal_close;

        modal_dialog.removeClass() .addClass('modal-dialog');
        modal_content.empty();

        modal_body  = $('<div class="modal-body"><span class="loader"></span></div>').appendTo(modal_content),
        modal_close = $('<a href="javascript:void(0)" onclick="return false;" class="modal-close" data-dismiss="modal"></a>').appendTo(modal_content);

        if(params && params.className) modal_dialog.addClass(params.className);

        modal.modal({
            show: true,
            keyboard: false,
            backdrop: 'static'
        });

        if(params && params.url) {
            $.get(params.url+((params.url.indexOf('?')+1) ? '&' : '?')+'isNaked=1', function(data){
                modal_body.html(data);
                modal_body.find('.selectpicker').selectric();
            });
        }

        if(params && params.html) {
            modal_body.html(params.html);
            modal_body.find('.selectpicker').selectric();
        }

        modal.on('hidden.bs.modal', function() {
            $(this).removeData('title');
            $(this).removeData('bs.modal');

            modal_content.empty();

            if(params && params.className) modal_dialog.removeClass(params.className);

            if(params && params.location) {
                switch(params.location) {
                    case 'reload':
                        window.location = window.location;

                        break;
                    default:
                        window.location = params.location;

                        break;
                }
            }
        });
    }

    function actions($this) {
        var actions = $this.data('action') || $this.data('onchange-action');

        $.each((typeof actions!='object' ? [actions] : actions), function(action, params) {
            switch(typeof action!='string' ? params : action) {
                case 'toggle-nav':
                    $('#nav').slideToggle();

                    break;
                case 'toggle-picker':
                    $('#picker .catalog-picker__content').slideToggle();

                    break;
                case 'toggle-filters':
                    $('#filters').slideToggle();

                    break;
                case 'show-modal':
                    showModal(params);

                    break;
            }
        });
    }

    $('body').prepend('<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"></div></div></div>');

    $('body').on('click', '[data-action]', function(event) {
        event.preventDefault();

        actions($(this));
    });

    $('.selectpicker').selectric();

    $(".owl-carousel").owlCarousel({
        items: 1
    });

    if($('#creditrange').length > 0) {
        $('#creditrange').slider({
            min: 0,
            max: 100,
            tooltip: 'always',
            formatter: function(val) {
                return val+'%';
            }
        });

        $('#creditrangeminus').click(function() {
            var val = $('#creditrange').slider('getValue');

            $('#creditrange').slider('setValue', val - 5);
        });

        $('#creditrangeplus').click(function() {
            var val = $('#creditrange').slider('getValue');

            $('#creditrange').slider('setValue', val + 5);
        });
    }

    $('[data-show-photo]').each(function() {
        $(this).hover(function() {
            var gallery = $('[data-gallery="car-photos"]');
            var images = gallery.find('[data-img]');

            images.addClass('hide');
            images.filter('[data-img="'+ $(this).data('show-photo') +'"]').removeClass('hide');
        });
    });

    $('[data-lightbox]').Chocolat();

    /** Disable zoom **/
    document.addEventListener('touchmove', function(event) {
        event = event.originalEvent || event;
        if(event.scale > 1) {
            event.preventDefault();
        }
    }, false);
});
